#coding: utf-8

# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
#    Authers:
#      -Dalai Felinto
#      -Yousef Harfoush
#      -Bastien Montagne
#      -Amin Babaeipanah (modified for Farsi support)
#
#    based on The Arabic Writer script by Omar Muhammad
#    thanks for Rabeh Torchi for ideas
#
# ***** END GPL LICENSE BLOCK *****

import sys
import os


def stripeol(s):
    return s.rstrip("\n\r")


# Isolated, Beginning, Middle, End
# http://en.wikipedia.org/wiki/Arabic_alphabet
# http://en.wikipedia.org/wiki/Arabic_characters_in_Unicode
alfmd = ["ﺁ","ﺁ","ﺂ","ﺂ"]
alfhz = ["ﺃ","ﺃ","ﺄ","ﺄ"]
wowhz = ["ﺅ","ﺅ","ﺆ","ﺆ"]
alfxr = ["ﺇ","ﺇ","ﺈ","ﺈ"]
hamzk = ["ﺉ","ﺋ","ﺌ","ﺊ"]
alfff = ["ﺍ","ﺍ","ﺎ","ﺎ"]
baaaa = ["ﺏ","ﺑ","ﺒ","ﺐ"]
peeee = ["ﭖ","ﭘ","ﭙ","ﭗ"]
tamrb = ["ﺓ","ﺓ","ﺔ","ﺔ"]
taaaa = ["ﺕ","ﺗ","ﺘ","ﺖ"]
thaaa = ["ﺙ","ﺛ","ﺜ","ﺚ"]
geeem = ["ﺝ","ﺟ","ﺠ","ﺞ"]
cheee = ["ﭺ","ﭼ","ﭽ","ﭻ"]
haaaa = ["ﺡ","ﺣ","ﺤ","ﺢ"]
khaaa = ["ﺥ","ﺧ","ﺨ","ﺦ"]
daaal = ["ﺩ","ﺩ","ﺪ","ﺪ"]
thaal = ["ﺫ","ﺫ","ﺬ","ﺬ"]
raaaa = ["ﺭ","ﺭ","ﺮ","ﺮ"]
zaaai = ["ﺯ","ﺯ","ﺰ","ﺰ"]
zheee = ["ﮊ","ﮊ","ﮋ","ﮋ"]
seeen = ["ﺱ","ﺳ","ﺴ","ﺲ"]
sheen = ["ﺵ","ﺷ","ﺸ","ﺶ"]
saaad = ["ﺹ","ﺻ","ﺼ","ﺺ"]
daaad = ["ﺽ","ﺿ","ﻀ","ﺾ"]
taaah = ["ﻁ","ﻃ","ﻄ","ﻂ"]
daaah = ["ﻅ","ﻇ","ﻈ","ﻆ"]
aayen = ["ﻉ","ﻋ","ﻌ","ﻊ"]
gayen = ["ﻍ","ﻏ","ﻐ","ﻎ"]
faaaa = ["ﻑ","ﻓ","ﻔ","ﻒ"]
qaaaf = ["ﻕ","ﻗ","ﻘ","ﻖ"]
kaaaf = ["ﮎ","ﮐ","ﮑ","ﮏ"]
gaaaf = ["ﮒ","ﮔ","ﮕ","ﮓ"]
laaam = ["ﻝ","ﻟ","ﻠ","ﻞ"]
meeem = ["ﻡ","ﻣ","ﻤ","ﻢ"]
nooon = ["ﻥ","ﻧ","ﻨ","ﻦ"]
hhhhh = ["ﻩ","ﻫ","ﻬ","ﻪ"]
wowww = ["ﻭ","ﻭ","ﻮ","ﻮ"]
yaamd = ["ﯼ","ﯾ","ﯿ","ﯽ"]
yaaaa = ["ﻱ","ﻳ","ﻴ","ﻲ"]
laamd = ["ﻵ","ﻵ","ﻶ","ﻶ"]
laahz = ["ﻷ","ﻷ","ﻸ","ﻸ"]
laaxr = ["ﻹ","ﻹ","ﻺ","ﻺ"]
laaaa = ["ﻻ","ﻻ","ﻼ","ﻼ"]

# defining numbers
numbers ="0123456789٠١٢٣٤٥٦٧٨٩"
#defining arabic unicodec chars
unicodec ="ﺁﺁﺂﺂﺃﺃﺄﺄﺅﺅﺆﺆﺇﺇﺈﺈﺉﺋﺌﺊﺍﺍﺎﺎﺏﺑﺒﺐﺓﺓﺔﺔﺕﺗﺘﺖﺙﺛﺜﺚﺝﺟﺠﺞﭺﭼﭽﭻﺡﺣﺤﺢﺥﺧﺨﺦﺩﺩﺪﺪﺫﺫﺬﺬﺭﺭﺮﺮﺯﺯﺰﺰﮊﮊﮋﮋﺱﺳﺴﺲﺵﺷﺸﺶﺹﺻﺼﺺﺽﺿﻀﺾﻁﻃﻄﻂﻅﻇﻈﻆﻉﻋﻌﻊﻍﻏﻐﻎﻑﻓﻔﻒﻕﻗﻘﻖﮎﮐﮑﮏﮒﮔﮕﮓﻝﻟﻠﻞﻡﻣﻤﻢﻥﻧﻨﻦﻩﻫﻬﻪﻭﻭﻮﻮﯼﯾﯿﯽﻱﻳﻴﻲﻵﻵﻶﻶﻷﻷﻸﻸﻹﻹﻺﻺﻻﻻﻼﻼ"

# letters that have only Isolated and End forms
# (and work as word breakers) + laaam and deriveds
wordbreak ="آأؤإاةدذرزوﻵﻷﻹﻻ"

# defining all arabic letters + harakat
arabic ="ًٌٍَُِّْْئءؤرلایةوزژظشسيبپلاتنمکگطضصثقفغعهخحچجدذْلآآلأأـلإإ،؟"
# defining the harakat
harakat ="ًٌٍَُِّْْ"
# defining other symbols
sym ="ًٌٍَُِّـ.،؟ @#$%^&*-+|\/=~(){}ْ,:"

def ProcessInput(input):
    """main function, the code is not self-explanatory.
    It requires understanding of arabic alphabet.
    """

    words = ""
    x=list(input)
    ln=len(x)

    #process each letter, submit it to tests and then add it to the output string
    # we can't do a for loop because we need to change 'g' inside the loop
    g = 0
    while g < ln: 

        b=a=1 #ignoring/discarding the harakat
        # see how many chars I need to skip to get the next
        # non-harakat char in the left (a) or the right (b)

        while g-b >= 0 and x[g-b] in harakat: b+=1
        while g+a < ln and x[g+a] in harakat: a+=1
        
        # get the position
        if x[g] not in wordbreak and g+a < ln and x[g+a] in arabic and x[g+a] != "ء":
            if g-b >= 0 and x[g-b] not in wordbreak and x[g-b] in arabic and x[g-b] != "ء":
                pos = 2 # middle
            else:
                pos = 1 # beggining
        else:
            if g-b >= 0 and x[g-b] not in wordbreak and x[g-b] in arabic and x[g-b] != "ء":
                pos = 3 # end
            else:
                pos = 0 # isolated

        # find what char to aggregate to the phrase based on the input and its
        # position in the word.
        chr = ""

        if x[g]=="\n": {} #if this char is a new line, go to add new line def
        elif x[g]=="\r": {} #if this char is carriage return, skip it.
        elif x[g]=="{": chr="}" #dealing with parenthesis
        elif x[g]=="}": chr="{"
        elif x[g]=="(": chr=")"
        elif x[g]==")": chr="("
        elif x[g]=="ء": chr="ﺀ"

        # dealing with letters, output each
        # letter with its right position
        elif x[g]=="آ": chr=alfmd[pos]
        elif x[g]=="أ": chr=alfhz[pos]
        elif x[g]=="ؤ": chr=wowhz[pos]
        elif x[g]=="إ": chr=alfxr[pos]
        elif x[g]=="ئ": chr=hamzk[pos]
        elif x[g]=="ا": chr=alfff[pos]
        elif x[g]=="ب": chr=baaaa[pos]
        elif x[g]=="پ": chr=peeee[pos]
        elif x[g]=="ة": chr=tamrb[pos]
        elif x[g]=="ت": chr=taaaa[pos]
        elif x[g]=="ث": chr=thaaa[pos]
        elif x[g]=="ج": chr=geeem[pos]
        elif x[g]=="چ": chr=cheee[pos]
        elif x[g]=="ح": chr=haaaa[pos]
        elif x[g]=="خ": chr=khaaa[pos]
        elif x[g]=="د": chr=daaal[pos]
        elif x[g]=="ذ": chr=thaal[pos]
        elif x[g]=="ر": chr=raaaa[pos]
        elif x[g]=="ز": chr=zaaai[pos]
        elif x[g]=="ژ": chr=zaaai[pos]
        elif x[g]=="س": chr=seeen[pos]
        elif x[g]=="ش": chr=sheen[pos]
        elif x[g]=="ص": chr=saaad[pos]
        elif x[g]=="ض": chr=daaad[pos]
        elif x[g]=="ط": chr=taaah[pos]
        elif x[g]=="ظ": chr=daaah[pos]
        elif x[g]=="ع": chr=aayen[pos]
        elif x[g]=="غ": chr=gayen[pos]
        elif x[g]=="ف": chr=faaaa[pos]
        elif x[g]=="ق": chr=qaaaf[pos]
        elif x[g]=="ک": chr=kaaaf[pos]
        elif x[g]=="گ": chr=gaaaf[pos]
        elif x[g]=="ل":
        # dealing with (la combination)
        # in this case the char has two chars in one
        # so we should increment the counter (g)
            g = g+1
            if g == ln:
                g = g-1
                chr=laaam[pos]
            elif x[g]=="ا": chr=laaaa[pos]
            elif x[g]=="أ": chr=laahz[pos]
            elif x[g]=="إ": chr=laaxr[pos]
            elif x[g]=="آ": chr=laamd[pos]
            else:
                g = g-1
                chr=laaam[pos]
        elif x[g]=="م": chr=meeem[pos]
        elif x[g]=="ن": chr=nooon[pos]
        elif x[g]=="ه": chr=hhhhh[pos]
        elif x[g]=="و": chr=wowww[pos]
        elif x[g]=="ی": chr=yaamd[pos]
        elif x[g]=="ي": chr=yaaaa[pos]
        elif x[g]=="لآ": chr=laamd[pos]
        elif x[g]=="لأ": chr=laahz[pos]
        elif x[g]=="لإ": chr=laaxr[pos]
        elif x[g]=="لا": chr=laaaa[pos]
        #if the char is a symbol, add it
        elif x[g] in sym:
            # Special cases for escaped \" and \t
            if x[g] == "\\" and g+1 < ln:
                if x[g+1] == "\"":
                    chr="\\\""
                    g += 1
                elif x[g+1] == "t":
                    chr="\\t"
                    g += 1
                else:
                    chr = x[g]
            else:
                chr = x[g]
        #if the char is an arabic reversed letter, reverse it back!
        elif x[g] in unicodec: chr=x[g]

        # advance to the next char
        g += 1
        # add the char before the previous one
        words = chr+words
    return words

def Start(fileR, fileW):
    """Open the .po file and do a special reverse in the msgstr lines"""
    fileR = open(fileR, "r",-1, "utf-8")
    fileW = open(fileW, "w",-1, "utf-8")

    inside_msgstr = False
    inside_header = True

    for line in fileR:
        if inside_header:
            fileW.write(line)
            if line == "\n": inside_header = False

        else:
            if line.startswith("msgstr"):
                    strng = stripeol(line)[8:-1]
                    rslt = ProcessInput(strng)
                    fileW.write("msgstr \"{}\"\n".format(rslt))
                    inside_msgstr = True
            elif inside_msgstr:
                strng = stripeol(line)[1:-1]
                if strng:
                    rslt = ProcessInput(strng)
                    fileW.write("\"{}\"\n".format(rslt))
                else:
                    fileW.write(line)
                    inside_msgstr = False
            else:
                fileW.write(line)
                inside_msgstr = False

    fileR.close()
    fileW.close()


if __name__ == "__main__":
    #argument parsing
    import argparse
    parser = argparse.ArgumentParser(description="Open the .po file and do " \
                                                 "a special reverse in the " \
                                                 "msgstr lines.")
    parser.add_argument('input', help="Input .po file.")
    parser.add_argument('output', default=None, help="Output .po file.")
    args = parser.parse_args()

    file_input = os.path.abspath(args.input)
    if not file_input.endswith(".po"):
        print("Error: Wrong file format. Use: `python3 ar_to_utf.py ar.po [ar_done.po]`")
        exit()

    # the last parameter is optional
    if args.output:
        file_output = os.path.abspath(args.output)
    else:
        file_output = "{}_done.po".format(file_input[:-3])

    # parse the file
    Start(file_input, file_output)

